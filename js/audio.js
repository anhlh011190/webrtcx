let openStream = ()=>{
    let constraints = {
        audio: true, 
        video: false
    };
	return navigator.mediaDevices.getUserMedia(constraints);
}

let playStream = (idAudioTag, stream) => {
	let audio = document.getElementById(idAudioTag);
    audio.srcObject = stream;
    setInterval(()=>{
        window.stream = stream;
        console.log(window.audioContext)
    },200);
}

try {
    window.AudioContext = window.AudioContext || window.webkitAudioContext;
    window.audioContext = new AudioContext();
} catch (e) {
    alert('Web Audio API not supported.');
}


openStream()
.then(stream => {
    playStream("localAudio", stream);
})
.catch((err) => {
    console.log(err.name)
});